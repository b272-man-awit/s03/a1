-- users
INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- posts
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (2, "First COde", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (2, "Second COde", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (4, "Third COde", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (5, "Fourth COde", "Bye bye solar system!", "2021-01-02 04:00:00");

-- Get all posts with auther ID of 2
SELECT user_id, content FROM posts WHERE user_id = 2; 

-- Get all user's email and datetime of creation
SELECT email, datetime_created from users;

-- update Hello earth
update posts SET content = "Hello to the people of the Earth!" where id=2;

-- delete johndoe
DELETE FROM users WHERE email = "johndoe@gmail.com";